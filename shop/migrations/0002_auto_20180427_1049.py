# Generated by Django 2.0.4 on 2018-04-27 10:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='good',
            name='img_content',
            field=models.ImageField(default='', help_text='картинка для переліку', upload_to='content/'),
        ),
        migrations.AlterField(
            model_name='good',
            name='goot_item',
            field=models.CharField(blank=True, choices=[('item_hit', 'item_hit'), ('item_action', 'item_action'), ('item_new', 'item_new'), ('', '')], default='', max_length=10),
        ),
    ]
