from django.urls import path

from . import views

app_name = 'shop'

urlpatterns = [
    path('', views.GoodsList.as_view(), name='index'),
    path('reg_user', views.UserRegistration.as_view(), name='user_registration'),
    path('<int:pk>', views.GoodDetail.as_view(), name='detail'),
    path('comment', views.CommentSave.as_view(), name='comment_save'),
    path('add_to_basket/<int:pk>', views.add_to_basket, name='add_to_basket'),
    path('remove_from_basket', views.del_good_from_basket, name='remove_good'),
    path('basket', views.BasketShow.as_view(), name='basket'),
    path('order', views.Order.as_view(), name='order'),
    path('oder_id/<int:pk>', views.show_order, name="show_order"),
]
