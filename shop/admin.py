from django.contrib import admin
from .models import Good, Comments, Order, GoodsInOrder

admin.site.register(Good)
admin.site.register(Comments)
admin.site.register(Order)
admin.site.register(GoodsInOrder)

