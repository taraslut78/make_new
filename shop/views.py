import json
from pprint import pprint

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core import serializers
from django.forms import formset_factory
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.views import generic
from .models import *
from django import forms


class TopGoodsCookiesMixin:
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=None, **kwargs)
        context['top_goods'] = Good.objects.all()[:5]
        quantity = 0
        summa = 0
        for item in self.request.session.keys():
            if 'good_' in item:
                q = int(self.request.session.get(item))
                try:
                    quantity += q
                    summa += q * Good.objects.get(pk=int(item[5:])).price
                except:
                    pass
        context['total_quantity'] = quantity
        context['sum'] = summa
        context['user_id'] = self.request.user.id
        try:
            context['top_comments'] = Good.objects.get(pk=self.kwargs['pk']).comments_set.all()[:3]
        except:
            pass
        context['order'] = False
        context['sum_and_send'] = summa + 500
        return context


class GoodsList(TopGoodsCookiesMixin, generic.ListView):
    model = Good
    paginate_by = 2


class GoodDetail(TopGoodsCookiesMixin, generic.DetailView):
    model = Good

    # http_method_names = ['get', 'post']

    def get_context_data(self, *, object_list=None, **kwargs):
        if self.request.GET.get('q'):
            self.request.session['good_' + str(self.kwargs['pk'])] = int(self.request.GET.get('q'))
        context = super().get_context_data(object_list=None, **kwargs)
        context['quantity'] = self.request.session.get('good_' + str(self.kwargs['pk']), 1)

        return context


def add_to_basket(request, pk):
    good = 'good_' + str(pk)
    quantity = request.session.get(good, 0) + int(request.GET.get('q'))
    request.session[good] = quantity
    return redirect('shop:index')


def del_good_from_basket(request):
    request.session.pop(request.GET.get('remove'))
    return redirect('shop:basket')


class CommentSave(LoginRequiredMixin, generic.CreateView):
    model = Comments
    form_class = CommemtForm

    def get_form_kwargs(self):
        kwards = super(CommentSave, self).get_form_kwargs()
        self.success_url = kwards['data']['url']
        return kwards

    def form_invalid(self, form):
        messages.success(self.request, "Треба щось писати")
        return redirect(self.success_url)


class BasketShow(TopGoodsCookiesMixin, generic.TemplateView):
    template_name = "shop/good_basket.html"

    def get_context_data(self, **kwargs):
        context = super(BasketShow, self).get_context_data(**kwargs)
        goods = []
        total_sum = 0
        for good_key in self.request.session.keys():
            if 'good_' not in good_key: continue
            good = Good.objects.get(pk=int(good_key[5:]))
            summ = good.price * int(self.request.session[good_key])
            total_sum += summ
            goods.append((good, int(self.request.session[good_key]), summ))
        context['goods'] = goods
        context['total_summ'] = total_sum
        context['order'] = True
        return context


class Order(TopGoodsCookiesMixin, LoginRequiredMixin, generic.CreateView):
    template_name = 'shop/good_order.html'
    model = Order
    form_class = FormOrder
    success_url = "shop:index"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['order'] = True
        return context

    def form_valid(self, form):
        pprint(form.cleaned_data)
        order = form.save()

        # save goods into DB
        good_key_list = list(self.request.session.keys())
        for good_key in good_key_list:
            if 'good_' not in good_key: continue
            good = Good.objects.get(pk=int(good_key[5:]))
            quantity = int(self.request.session.get(good_key))
            try:
                GoodsInOrder.objects.create(order=order, good=good, quantity=quantity)
                del self.request.session[good_key]
            except:
                pass
        return super().form_valid(form)


class FormUserRegistration(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        exclude = ['groups', 'user_permissions', 'is_staff', 'is_superuser',
                   'is_active', 'date_joined', 'last_login', 'last_name']


class UserRegistration(TopGoodsCookiesMixin, generic.CreateView):
    form_class = FormUserRegistration
    # model = User
    template_name = "shop/user_registration.html"
    success_url = '/'

    def form_valid(self, form):
        user = form.save(commit=False)
        user.set_password(form.cleaned_data['password'])
        user.save()
        print(user)
        User.objects.create(user=user)


def show_order(request, pk):
    properties_from_order = list(('name', 'price', 'quantity',))
    goods_in_order = GoodsInOrder.objects.filter(order_id=pk). \
        values_list('good__name', 'good__price', 'quantity')
    rez = [dict(zip(properties_from_order, i)) for i in list(goods_in_order)]
    return JsonResponse(rez, safe=False)
